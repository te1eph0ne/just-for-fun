#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>
#include <curl/curl.h>

#define AUTHOR "Phone <denwa@null.net>"
#define DEBUG

struct string {
  char* ptr;
  size_t sze;
};

size_t getdata(char *buffer, size_t size, size_t nitems, struct string* userdata);

int main(int argc, const char** argv) {
  int i;
  // Help flag
  int h = 0;
  // English flag
  int e = 0;
  // Decide to like, display the help dialog
  // or something else
  if(argc != 1) {
    for(i = 0; i < argc; i++) {
      if(!(strcmp(argv[i], "-h")) || !(strcmp(argv[i], "--help"))) {
	h = 1;
      }
      else if(!(strcmp(argv[i], "--en"))) {
	e = 1;
      }
      // Override -h when cool flags are enabled
      if(e) {
	if(h)
	  h = 0;
	break;
      }
    }
  } else {
    h = 1;
  }
  if(h) {
    printf("%s - downloads a video from animatorexpo.com\nhacked together by "
	   AUTHOR" in early October 2015\nusage: %s <flags> <url>\n\
flags:\n\t--en\tuse english subtitles\n\t-h\n\t--help\tdisplay this dialog\n", argv[0], argv[0]);
  } else {
    // Copy string at argv[argc - 1] to stack and assign
    // label "url"
    for(i = 0; argv[argc - 1][i]; i++) {}
    char url[i+1];
    for(i = 0; i < (int)(sizeof(url)/sizeof(url[0])); i++) {
      url[i] = argv[argc - 1][i];
    }
    url[sizeof(url)/sizeof(url[0])] = '\0';
    regex_t exp;
    if(regcomp(&exp, "^http:\\/\\/(w{3}\\.)?([a-z\\./]+)$", REG_EXTENDED)) {
      printf("error: failed to compile regular expression\n");
      exit(EXIT_FAILURE);
    }
    if(regexec(&exp, url, 0, NULL, 0) == REG_NOMATCH) {
      printf("error: that url isn't a url, fuckwit\n");
      exit(EXIT_FAILURE);
    } else {
      curl_global_init(CURL_GLOBAL_ALL);
      CURL* curl;
      struct string s;
      s.sze = 0;
      s.ptr = malloc(s.sze + 1);
      s.ptr[0] = '\0';
      curl = curl_easy_init();
      curl_easy_setopt(curl, CURLOPT_URL, url);
      curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, getdata);
      curl_easy_setopt(curl, CURLOPT_WRITEDATA, &s);
      if(curl_easy_perform(curl) == CURLE_OK) {
#ifdef DEBUG
	printf("successfully fetched resource at %s\n", url);
#endif
      } else {
	printf("error: failed to fetch resource at %s\n", url);
	exit(EXIT_FAILURE);
      }
      curl_easy_cleanup(curl);
      // Initialise string for new url
      struct string newurl;
      newurl.sze = 0;
      newurl.ptr = malloc(s.sze + 1);
      newurl.ptr[0] = '\0';

      regmatch_t regmatch[1];
      // Begin forging url
      strcpy(newurl.ptr,
	     "http://ext.nicovideo.jp/api/video_play_info?video_id=");
      if(!e) {
	if(regcomp(&exp, "\"ja\":\[[a-z0-9\",]+]", REG_EXTENDED)) {
	  printf("error: failed to compile regular expression\n");
	  exit(EXIT_FAILURE);
	}
      } else {
	if(regcomp(&exp, "\"en\":\[[a-z0-9\",]+]", REG_EXTENDED)) {
	  printf("error: failed to compile regular expression\n");
	  exit(EXIT_FAILURE);
	}
      }
      if(regexec(&exp, s.ptr, 1, regmatch, 0) == REG_NOMATCH) {
	printf("error: invalid animatorexpo.com video page\n");
	exit(EXIT_FAILURE);
      }
      char vars[(regmatch[0].rm_eo - regmatch[0].rm_so) ];
      strncpy(vars,
	      s.ptr + regmatch[0].rm_so,
	      regmatch[0].rm_eo - regmatch[0].rm_so
	      );
      vars[sizeof(vars) / sizeof(char)] = '\0';
      printf("%s\n%s\n", newurl.ptr, vars);
      
      //curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, getdata);
      //curl_easy_setopt(curl, CURLOPT_HEADERDATA, &s);
    }
  }
  curl_global_cleanup();
  return 0;
}

size_t getdata(char *buffer, size_t size, size_t nitems, struct string* userdata) {
  userdata->ptr = realloc(userdata->ptr, (size * nitems) + userdata->sze);
  if(!(userdata->ptr)) {
    printf("error: failed to reallocate memory");
    exit(EXIT_FAILURE);
  } else {
    memcpy(userdata->ptr+userdata->sze, buffer, size * nitems);
    userdata->sze = (size * nitems) + userdata->sze;
    userdata->ptr[userdata->sze] = '\0';
#ifdef DEBUG
    printf("successfully copied %lu bytes from buffer\n", size * nitems);
#endif
  }
  return size * nitems;
}
