#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

void ResetDialogue();
void HighScore();
void Judges(unsigned hs);
void Score(unsigned j, unsigned hs);
void AskRestart();

int main(void) {
  ResetDialogue();
}

void ResetDialogue() {
  puts("\x1b[32mWelcome to the score average calculator.\n\
Please make sure your scores have the same top score(*/10).\x1b[0m");
  HighScore();
}

void HighScore() {
  char hs[11];
  puts("What is the highest possible score?");
  fgets(hs, 11, stdin);
  if(atoi(hs) > 0)
    Judges(atoi(hs));
  else {
    puts("Invalid highscore.");
    HighScore();
  }
}

void Judges(unsigned hs) {
  char j[11];
  puts("Please enter the amount of Judges.");
  fgets(j, 11, stdin);
  if(atoi(j) > 0)
    Score(atoi(j), hs);
  else {
    puts("Invalid number of judges.");
    Judges(hs);
  }
}

void Score(unsigned j, unsigned hs) {
  char inp[11];
  double av = 0;
  for(unsigned i = 0; i < j; i++) {
    printf("Please input score #%u.\n", i + 1);
    fgets(inp, 11, stdin);
    if(strtod(inp, NULL) <= hs && strtod(inp, NULL) >= 0)
      av += strtod(inp, NULL);
    else {
      puts("Invalid score.");
      i--;
    }
  }
  av /= j;
  printf("Your average score is %.1f.\n", av);
  AskRestart();
}

void AskRestart() {
  puts("Would you like to average another score? (Y/N)");
  switch(toupper(getchar())) {
  case 'Y':
    fpurge(stdin);
    ResetDialogue();
  case 'N':
    break;
  default:
    puts("Invalid choice.");
    AskRestart();
  }
}
